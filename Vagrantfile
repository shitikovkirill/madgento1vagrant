# encoding: utf-8
# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

current_dir = File.dirname(File.expand_path(__FILE__))
configs     = YAML.load_file("#{current_dir}/config.yaml")
m_id        = configs['magento']['id']
m_token     = configs['magento']['token']
root_p      = configs['mariadb']['root_p']

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"

  config.vm.network "private_network", ip: "192.168.10.102"
  config.vm.network "forwarded_port", guest: 80, host: 8080
  
  config.vm.provision "shell", inline: <<-SHELL
    set -ex
    MAGENTO_ID=#{m_id}
    MAGENTO_TOKEN=#{m_token}
    curl -k https://$MAGENTO_ID:$MAGENTO_TOKEN@www.magentocommerce.com/products/downloads/info/help
    # curl -k https://$MAGENTO_ID:$MAGENTO_TOKEN@www.magentocommerce.com/products/downloads/info/filter/version/1.*
    curl -k https://$MAGENTO_ID:$MAGENTO_TOKEN@www.magentocommerce.com/products/downloads/info/filter/version/1.9.2.1/type/ce-full
  
    apt-get update
    apt-get install -y aptitude
    apt-get install -y nginx
    
    echo "Mariadb"
    apt-get install -y mariadb-server mariadb-client
    chmod +x /vagrant/script/mysql_secure.sh
    /vagrant/script/mysql_secure.sh '#{root_p}'
    
    echo "Install php"
    apt-get install -y software-properties-common python-software-properties
    add-apt-repository -y ppa:ondrej/php
    apt-get update
    apt-get install -y php7.0-fpm php7.0-common php7.0-mbstring php7.0-xmlrpc php7.0-soap php7.0-gd php7.0-xml php7.0-intl php7.0-mysql php7.0-cli php7.0-ldap php7.0-zip php7.0-curl
    
    echo "Create db"
    mysql -u root -e "CREATE DATABASE IF NOT EXISTS magento;" -v
    mysql -u root -e "CREATE DATABASE IF NOT EXISTS safespro;" -v
    mysql -u root -e "update mysql.user set plugin = 'mysql_native_password' where User='root'; FLUSH PRIVILEGES;" -v
    
    echo "Install magento"
    mkdir -p /var/www/html/
    wget https://$MAGENTO_ID:$MAGENTO_TOKEN@www.magentocommerce.com/products/downloads/file/magento-1.9.2.1.tar.gz
    tar -zxvf magento-1.9.2.1.tar.gz -C /var/www/html/
    tar -zxvf /vagrant/safespro.tar.gz -C /var/www/html/
    chown -R www-data:www-data /var/www/html/
    chmod -R 755 /var/www/html/
    
    ln -s /vagrant/nginx/magento.conf /etc/nginx/sites-enabled/
    ln -s /vagrant/nginx/safes.conf /etc/nginx/sites-enabled/
    nginx -t
    nginx -s reload
    echo ";listen = 127.0.0.1:9000" >> /etc/php/7.0/fpm/php-fpm.conf
    echo "listen = /var/run/php-fpm.sock" >> /etc/php/7.0/fpm/php-fpm.conf
    systemctl reload php7.0-fpm

    echo "Done"
  SHELL
end
